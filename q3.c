/********************
	Include
********************/

#include<stdio.h>
#include<unistd.h>
#include<stdlib.h>
#include <semaphore.h>
#include<pthread.h>

void *producer (void *arg);		/*** Function 1 Declaration ***/
void *consumer (void *arg1);		/*** Function 2 Declaration ***/
sem_t p;
int bfr[16];

/**** Main ****/

int main()
{
	int a,b,c,r;
	pthread_t x,y;
	
	/**** SEMAPHORE INITIALIZATION ****/
	
	r = sem_init(&p, 0, 1);
	if (r == 0) 
	{
		printf("Semaphore initialization Successfull.\n");
	}
	
	/**** THREAD 1 CREATION ****/
	
	a = pthread_create (&x, NULL, producer, NULL);
	pthread_join(x, NULL);
	 
	/**** THREAD 2 CREATION ****/
	
	b = pthread_create (&y, NULL, consumer, NULL);
	pthread_join(y, NULL);
	
	if (b == a == 0 ) 
	{
		printf("Thread creation successfull.\n");
	}
	
	sem_destroy(&p);
	printf("\n");
	return 0;
}
	
	/*** Function 1 Initialization ***/
	
	void *producer (void *arg)
	{
		sem_wait (&p);
		int z;
		printf("Enter number/numbers (integer) as this is producer.\n");
		
		for (z=0; z<16; z++)
		{
			scanf("%d\n",&bfr[z]);
		}
		sem_post (&p);
	}
	
	/*** Function 2 Initialization ***/
	
	void *consumer (void *arg1)
	{
		sem_wait (&p);
		int z;
		printf("Reading number/numbers (integer) entered by producer as this is consumer.\n");
		
		for (z=0; z<16; z++)
		{
			printf("%d\t",bfr[z]);
		}
		sem_post (&p);
	}
	
		
			
	
	
	
	
	
	
	
