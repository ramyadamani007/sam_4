/********************
	Include
********************/

#include<stdio.h>
#include<unistd.h>
#include<stdlib.h>
#include<pthread.h>

void *producer (void *arg);		/*** Function 1 Declaration ***/
void *consumer (void *arg1);		/*** Function 2 Declaration ***/
pthread_mutex_t p;
int bfr[16];

/**** Main ****/

int main()
{
	int a,b,c,r;
	pthread_t x,y;
	
	/**** Mutex INITIALIZATION ****/
	
	r = pthread_mutex_init(&p, NULL);
	if (r == 0) 
	{
		printf("Mutex initialization Successfull.\n");
	}
	
	/**** THREAD 1 CREATION ****/
	
	a = pthread_create (&x, NULL, producer, NULL);
	pthread_join(x, NULL);
	 
	/**** THREAD 2 CREATION ****/
	
	b = pthread_create (&y, NULL, consumer, NULL);
	pthread_join(y, NULL);
	
	if (b == a == 0 ) 
	{
		printf("Thread creation successfull.\n");
	}
	
	pthread_mutex_destroy(&p);
	printf("\n");
	return 0;
}
	
	/*** Function 1 Initialization ***/
	
	void *producer (void *arg)
	{
		pthread_mutex_lock(&p);
		int z;
		printf("Enter number/numbers (integer) as this is producer.\n");
		
		for (z=0; z<16; z++)
		{
			scanf("%d\n",&bfr[z]);
		}
		pthread_mutex_unlock(&p);
	}
	
	/*** Function 2 Initialization ***/
	
	void *consumer (void *arg1)
	{
		pthread_mutex_lock(&p);
		int z;
		printf("Reading number/numbers (integer) entered by producer as this is consumer.\n");
		
		for (z=0; z<16; z++)
		{
			printf("%d\t",bfr[z]);
		}
		pthread_mutex_unlock(&p);
	}
	
	
	
	
	
	
	
	
	
	
	
